#include <stdio.h>
#include <limits.h>

int main ()
{
    printf("The size of a char is: %d bits\n", CHAR_BIT);
    printf("Max and Min range of char: (%d)-(%d)\n", CHAR_MAX, CHAR_MIN);
    printf("The size of a int is: %dbits\n", sizeof(int )*8);
    printf("Max and Min range of int: (%d)-(%d)\n", INT_MAX, INT_MIN);
    printf("The size of a short is: %dbits\n", sizeof(short)*8);
    printf("Max and Min range of short: (%d)-(%d)\n", SHRT_MAX, SHRT_MIN);
    
    
    return 0;
}
