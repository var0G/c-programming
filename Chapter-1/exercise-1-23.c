#include <stdio.h>

#define IN 1
#define OUT 0

int main() 
{
    int  c, state;

    state = OUT;
    while ((c = getchar()) != EOF) {
        if (c == '/') {
            state = IN;
        }
        if (state == IN && c == '\n')
            state = OUT;
        if (state == OUT) {
            putchar(c);
        }
    }
    return 0;
}
