#include <stdio.h>

#define MAXLINE 1000
#define MAXCOLUMN 40

int fold(char line[MAXLINE]);

int main()
{
    char l[MAXLINE];

    while (fold(l) >  0) {
	    printf("%s", l);
    }

    return 0;
}

int fold(char line[MAXLINE]) 
{
    int c, i;

    i = 0;
    while ((c = getchar()) != EOF && c != '\n' && i < MAXCOLUMN) {
        line[i] = c;
        ++i;
    }
    if (i == MAXCOLUMN && line[i-1] != ' ' && line[i+1] != '\n') {
        line[i] = '\\';
        ++i;
        line[i] = '\n';
        ++i;
        line[i] = '\0';
    } else if (i == MAXCOLUMN && line[i-1] == ' ') {
        line[i-1] = '\\';
        ++i;
        line[i-1] = '\n';
    }
    if (c == '\n' || line[i-1] != '\0' && i != MAXCOLUMN) {
        line[i] = c;
        ++i; 
        line[i] = '\0';
    }

    return i;
}
