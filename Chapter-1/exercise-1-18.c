#include <stdio.h>

#define MAXLINE 1000

int get_line(char line[], int maxline);

int main()
{
    int len;
    char line[MAXLINE];

    while ((len = get_line(line, MAXLINE)) > 0) {
        if (len > 1) {
            printf("%s", line);
        } else {
            continue;
        }
    }

    return 0;
}

int get_line(char s[], int lim) 
{
    int c, i;

    i = 0;
    while (i < lim-1 && (c = getchar()) != EOF && c != '\n') {
        if (c == '\t' || c == ' ') {
            continue; 
        } else {
            s[i] = c;
            ++i; 
        }
    }
    if (c == '\n' || s[i-1] != '\0') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';

    return i;
}

