#include <stdio.h>

int main()
{ 
    int i, c, count, top;
    int chars[11]; 

    top = 0; 
    count = 0; 
    
    for (i = 0; i < 82; i++) {
        chars[i] = 0;
    }
    
    while ( (c = getchar()) != EOF) {
        if (c >= '!' && c <= '.') {
            if (count > 0 && count < 11) {
                chars[count-1]++;
            } 
            count = 0; 
        }
        else {
                count++;
        }
    }

    for (i = 0; i < 11; i++) {
        printf("Frecuency:%d = %d \n ", i+1, chars[i]);
    }

    printf("\n");

    top = chars[0];
    for (i = 0; i < 11; i++) {
        if (chars[i] > top) top = chars[i];
    }
    
    while (top > 0) {
        for (i = 0; i < 11 ; i++) {
            if ( chars[i] < top) {
                printf("\t");
            }
            else {
                printf("*\t"); 
            }
        }
        printf("\n");
        top--;
    }
    for (i = 0; i < 11; i++){
        printf("%d\t", i+1);
    } 
    printf("\n");
    return 0;
}
