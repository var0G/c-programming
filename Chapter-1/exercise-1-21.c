#include <stdio.h>


int main()
{
    int c,  i, count;

    count = 0;
    while ((c = getchar()) != EOF) {
        if (c == ' ') {
            ++count;
            continue;
        }
        if (count >= 4) {
            for (i = 0; i< (int)count/4; ++i)
               putchar('\t');
            count = 0;
        }
        if (c != ' ' && count > 0 && count < 4) {
           for (i = 0; i< count; ++i)
                putchar(' ');
           count = 0;
        }
        putchar(c);
        count = 0;
    }
    return 0;
}

