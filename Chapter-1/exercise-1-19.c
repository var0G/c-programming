#include <stdio.h>

#define MAXLINE 1000

int get_line(char line[], int maxline);
void reverse( char reverse_string[]);

int main() {

    int len;
    char line[MAXLINE];

    while ((len = get_line(line, MAXLINE)) > 0) {
        if (len) {
            reverse(line);
            printf("%s", line);
        } 
    }

    return 0;
}

int get_line(char s[], int lim)
{

    int c, i;

    i = 0;
    while (i < lim-1 && (c = getchar()) != EOF && c != '\n') {
        s[i] = c;
        ++i; 
    }
    if (c == '\n' || s[i-1] != '\0') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';

    return i;
}

void reverse(char reverse_string[]) 
{

   int i = 0, j = 0, aux = 0; 

    for (i = 0; reverse_string[i] != 0; ++i); 
        for (j = 0; j < i/2; ++j) {
            aux = reverse_string[j]; 
            reverse_string[j] = reverse_string[i-2-j];
            reverse_string[i-2-j] = aux;
        }
    ++i;
    reverse_string[i] = '\n';
    ++i;
    reverse_string[i] = '\0';

}

