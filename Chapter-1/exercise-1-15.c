#include <stdio.h>

float fahrenheit_convert( int fahr);

int main()
{
    int i;

    for (i = 0; i < 10; ++i)
        printf("\t%d\t %6.1f\n",i , fahrenheit_convert(i));

    return 0;
}

float fahrenheit_convert(int fahr) 
{

    float celcius;

    celcius = (5.0/9.0) * (fahr-32.0);

    return celcius;
}


