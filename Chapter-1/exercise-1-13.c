#include <stdio.h>

int main()
{
    int c, i, count, top;
    int words[11];
    
    count = 0;
    top = 0;

    for (i = 0; i < 11; i++) {
        words[i] = 0;
    }
    
    while (( c = getchar()) != EOF) {
        if (c == ' ' || c == '\n' || c == '\t') { 
            if (count > 0 && count < 11)
            {
                words[count-1]++;
            } 
            count = 0; 
        }
        else { 
            count++;
        }
    }

    for (i = 0; i < 11; i++) {
        printf("Frecuency:%d = %d \n ", i+1, words[i]);
    }

    printf("\n");

    top = words[0];
    for (i = 0; i < 11; i++) {
        if (words[i] > top) top = words[i]; 
    }
    
    while (top > 0) {
        for (i = 0; i < 11 ;i++) {
            if (words[i] < top) {
                printf("\t");
            }
            else {
                printf("*\t"); 
            }
        }
        printf("\n");
        top--;
    }
    for (i = 0; i < 11; i++) {
        printf("%d\t", i+1);
    } 
    printf("\n");

}
